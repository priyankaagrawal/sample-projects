package com.concretepage.web;


import com.concretepage.core.LoggedInUserDetails;

import com.concretepage.model.User;
import com.concretepage.model.UserRole;

import com.concretepage.repository.UserRepository;
import com.concretepage.repository.UserRoleRepository;
import com.concretepage.service.UserService;

import com.concretepage.utils.CoreUtils;
import org.apache.commons.collections.IteratorUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class LoginController {

    @Inject
    UserService userService;
    @Inject
    UserRepository userRepository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        return "redirect:/index.html";
    }

    @Inject
    UserRoleRepository userRoleRepository;

    @RequestMapping(value = "/access/change-password", produces = "application/json; charset=utf-8")
    @ResponseBody
    public Map<String, Object> changePassword(@RequestParam String oldPassword, @RequestParam String newPassword,
                                              @RequestParam(value = "username", required = false, defaultValue = "") String username) {
        Map<String, Object> map = new HashMap<String, Object>();
        User user = userService.getLoggedInUser();
        if (userRoleRepository.findByUser(user).getType().equalsIgnoreCase(UserRole.ROLE_ADMIN) &&
                !username.trim().equalsIgnoreCase("") && !username.equalsIgnoreCase(user.getUsername())) {
            user = userRepository.findByUsername(username);
            if (user == null) {
                map.put("status", "error");
                map.put("message", "User not found");
                return map;
            }
            user.setPassword(CoreUtils.encodePassword(newPassword));
            user.setIsEncrypted(1);
            userRepository.save(user);
            map.put("status", "success");
            map.put("message", "Password Changed Successfully");
            return map;
        }
        if (CoreUtils.matchPassword(oldPassword, user.getPassword())) {
            user.setPassword(CoreUtils.encodePassword(newPassword));
            user.setIsEncrypted(1);
            userRepository.save(user);
            map.put("status", "success");
            map.put("message", "Password Changed Successfully");
        } else {
            map.put("status", "error");
            map.put("message", "Incorrect Password");
        }
        return map;
    }


    @RequestMapping(value = "/authenticate", produces = "application/json; charset=utf-8")
    @ResponseBody
    @Transactional
    public Map<String, Object> authenticate(Principal principal) {
//        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> model = new HashMap<>();

        model.put("status", "success");
        model.put("message", "Login Successful");
        model.put("user", CoreUtils.objectToJson(principal));
        return model;
    }

    @RequestMapping(value = "/login-success", produces = "application/json; charset=utf-8")
    @ResponseBody
    @Transactional
    public Map<String, Object> loginSuccess(Principal user) {
        Map<String, Object> model = new HashMap<>();

        model.put("status", "success");
        model.put("message", "Login Successful");
        model.put("user", CoreUtils.objectToJson(user));

        return model;
    }

    @RequestMapping(value = "/login-failure", produces = "application/json; charset=utf-8")
    @ResponseBody
    public Map<String, Object> userFailure(HttpSession session) {
        Map<String, Object> model = new HashMap<String, Object>();
        User user=userRepository.findByUsernameAndPassword("1","1");
        model.put("status", "error");
        model.put("message", ((Exception) session.getAttribute("SPRING_SECURITY_LAST_EXCEPTION")).getMessage());
        return model;
    }


    @RequestMapping(value = "/check-web", produces = "application/json; charset=utf-8")
    @ResponseBody
    @Transactional
    public Map<String, Object> authenticateWeb(Principal principal) {
        Map<String, Object> map = new HashMap<>();
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = (UsernamePasswordAuthenticationToken) principal;
        LoggedInUserDetails loggedInUserDetails = (LoggedInUserDetails) usernamePasswordAuthenticationToken.getPrincipal();
        User user = userRepository.findByUsername(loggedInUserDetails.getUsername());
        List<UserRole> userRoles = IteratorUtils.toList(user.getUserRoles().iterator());
//        System.out.println("type===" + userRoles.get(0).getType());
//        if (userRoles.get(0).getType().equalsIgnoreCase(UserRole.ROLE_ADMIN)
//                || userRoles.get(0).getType().equalsIgnoreCase(UserRole.ROLE_USER)
//                ) {
//
//            Surveyor surveyor = surveyorRepository.findByUser(user);
//            map.put("loginData", surveyor);
//            map.put("surveyorId", surveyor.getId());
//
//        } else {
//            Respondent respondent = respondentRepository.findByUser(user);
//            //Since, account is lazy giving it directly to jackson for serlization didn't worked & hence, this quick-fix.
//            map.put("loginData", respondent);
//            map.put("respondentId", respondent.getId());
//        }
        map.put("user", user);

        return map;
    }

    @RequestMapping(value = "/user-web", produces = "application/json; charset=utf-8")
    @ResponseBody
    @Transactional
    public Map<String, Object> getUser() {
        Map<String, Object> map = new HashMap<>();

        User user = userService.getLoggedInUser();
        List<UserRole> userRoles = IteratorUtils.toList(user.getUserRoles().iterator());
//        if (userRoles.get(0).getType().equalsIgnoreCase(UserRole.ROLE_ADMIN)
//                || userRoles.get(0).getType().equalsIgnoreCase(UserRole.ROLE_USER)
//                ) {
//
//            Surveyor surveyor = surveyorRepository.findByUser(user);
//            map.put("loginData", surveyor);
//            map.put("respondentId", 0);
//
//        } else {
//            Respondent respondent = respondentRepository.findByUser(user);
//            //Since, account is lazy giving it directly to jackson for serlization didn't worked & hence, this quick-fix.
//            map.put("loginData", respondent);
//            map.put("respondentId", respondent.getId());
//        }

        map.put("user", user);

        return map;
    }

    @RequestMapping(value = "/encode-pass", produces = "application/json; charset=utf-8")
    @ResponseBody
    @Transactional
    public Map<String, Object> encode() {
        Map<String, Object> model = new HashMap<>();
        List<User> lstUser = userRepository.findByIsEncrypted(0);
        for (User user : lstUser) {
            user.setPassword(CoreUtils.encodePassword(user.getPassword()));
            user.setIsEncrypted(1);
            userRepository.save(user);
        }
        model.put("status", "success");
        model.put("message", "Login Successful");

        return model;
    }


}
