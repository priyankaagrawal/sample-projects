package com.concretepage.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Data
@NoArgsConstructor
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer","clientPackage"})
@Table(name = "client_package_center")
public class ClientPackageCenter implements Serializable{

    @Id
    @Column(unique = true, name = "id")
    private Integer id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "clientPackage", nullable = false)
    private ClientPackage clientPackage;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "center", nullable = false)
    private Center center;

}
