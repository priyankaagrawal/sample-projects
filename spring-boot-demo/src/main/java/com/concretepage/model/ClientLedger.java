package com.concretepage.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer", "client"})
@Table(name = "client_ledger")
public class ClientLedger implements Serializable{

    @Id
    @Column(unique = true, name = "id")
    private Integer id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client", nullable = false)
    private Client client;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "invoice", nullable = false)
    private Invoice invoice;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "receipt", nullable = false)
    private Invoice receipt;
    @Column(name = "cr")
    private Double cr;
    @Column(name = "dr")
    private Double dr;
    @Column(name = "status")
    private LedgerStatusEnum status;
    @Column(name = "transaction_date")
    private Date transactionDate;

    @Column(name = "last_modified")
    private Date lastModified = new Date();

    @Column(name = "remark")
    private String reamrk;


}
