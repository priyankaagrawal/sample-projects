package com.concretepage.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer", "invoices","estimates"})
@Table(name = "billing_firm")
public class BillingFirm implements Serializable {

    @Id
    @Column(unique = true, name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;
    @Column(name = "address")
    private String address;
    @Column(name = "email")
    private String email;
    @Column(name = "registration_number")
    private String registrationNumber;
    @Column(name = "gstin")
    private String gstin;
    @Column(name = "sac")
    private String sac;
    @Column(name = "creation_date")
    private Date creationDate;

    @Column(name = "last_modified")
    private Date lastModified = new Date();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "billingFirm")
    private List<FirmBank> firmBanks = new ArrayList<FirmBank>();


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "billingFirm")
    private List<Invoice> invoices = new ArrayList<Invoice>();


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "billingFirm")
    private List<Estimate> estimates = new ArrayList<Estimate>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "billingFirm")
    private List<BillingFirmTax> billingFirmTaxes = new ArrayList<BillingFirmTax>();

}
