package com.concretepage.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer", "billingFirmTaxes","invoiceTaxes","estimateTaxes"})
@Table(name = "tax")
public class Tax implements Serializable{
    @Id
    @Column(unique = true, name = "id")
    private Integer id;
    @Column(name="name")
    private String name;
    @Column(name="percentage")
    private Double percentage;
    @Column(name="status")
    private StatusEnum statusEnum;



    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tax")
    private List<EstimateTax> estimateTaxes = new ArrayList<EstimateTax>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tax")
    private List<InvoiceTax> invoiceTaxes = new ArrayList<InvoiceTax>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "tax")
    private List<BillingFirmTax> billingFirmTaxes = new ArrayList<BillingFirmTax>();
}
