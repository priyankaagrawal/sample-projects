package com.concretepage.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer", "billingFirm"})
@Table(name = "firmBank")
public class FirmBank implements Serializable {
    @Id
    @Column(unique = true, name = "id")
    private Integer id;
    @Column(name="bank")
    private String bank;
    @Column(name="branch")
    private String branch;
    @Column(name="account_no")
    private String accountNo;
    @Column(name="account_name")
    private String accountName;
    @Column(name="ifsc")
    private String ifsc;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "billing_firm", nullable = false)
    private BillingFirm billingFirm;

}
