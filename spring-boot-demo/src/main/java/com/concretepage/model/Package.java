package com.concretepage.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer", "clientPackages"})
@Table(name = "package")
public class Package implements Serializable {

    @Id
    @Column(unique = true, name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "no_of_lead")
    private Integer noOfleads;

    @Column(name = "recovery_lead")
    private Integer recoveryLeads;

    @Column(name = "offer_lead")
    private Integer offerLead;

    @Column(name = "replacement")
    private Integer replacement;

    @Column(name = "rate_per_lead")
    private Double ratePerLead;

    @Column(name = "amount")
    private Double amount;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "packages")
    private List<ClientPackage> clientPackages = new ArrayList<ClientPackage>();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "product", nullable = false)
    private Product product;
}
