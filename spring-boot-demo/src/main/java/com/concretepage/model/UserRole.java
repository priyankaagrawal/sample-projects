package com.concretepage.model;

/**
 * Created by Priyanka on 06-03-2015.
 */

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@EqualsAndHashCode(of={"id"})
@JsonIgnoreProperties({"user"})
@Table(name = "user_role")
public class UserRole implements Serializable {

    private static final long serialVersionUID = 1L;
    public final static String ROLE_ADMIN = "ROLE_ADMIN";
    public final static String ROLE_USER = "ROLE_USER"; // Sales Manager
    public final static String ROLE_RESPONDENT = "ROLE_RESPONDENT";
//    public final static String ROLE_DISPATCH = "ROLE_DISPATCH";


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "default_role")
    private Boolean defaultRole;

    @Column(name = "type")
    private String type;

    @Column(name = "permission")
    private String permission;

    @Column(name = "created_date")
    @NotNull(message = "User Creation Date cannot be null")
    private Date createdDate= new Date();

    @Column(name = "status")
    private Integer status;

    private String desription;

}
