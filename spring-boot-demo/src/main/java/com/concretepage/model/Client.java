package com.concretepage.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer", "clientPackages","clientLedgers","estimates"})
@Table(name = "clinic")
public class Client implements Serializable{
    @Id
    @Column(unique = true, name = "id")
    private Integer id;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client")
    private List<Center> centers = new ArrayList<Center>();

    @Column(name = "ownwer_name")
    private String ownwerName;

    @Column(name = "ownwer_specialization")
    private String ownerSpecialization;

    @Column(name = "owner_mobile1")
    private String ownwerMobile1;

    @Column(name = "ownwer_mobile2")
    private String ownwerMobile2;

    @Column(name = "ownwer_email")
    private String ownwerEmail;

    @Column(name = "contact_name")
    private String contactName;

    @Column(name = "contact_designation")
    private String contactDesignation;

    @Column(name = "contact_mobile1")
    private String contactMobile1;

    @Column(name = "contact_mobile2")
    private String contactMobile2;

    @Column(name = "contact_email")
    private String contactEmail;


    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "mobile")
    private String mobile;

    @Column(name = "alt_mobile")
    private String altMobile;
    @Column(name = "website")
    private String website;
    @Column(name = "landline")
    private String landline;
    @Column(name = "address")
    private String address;
    @Column(name = "city")
    private String city;
    @Column(name = "country")
    private String country;
    @Column(name = "state")
    private String state;
    @Column(name = "notes")
    private String notes;
    @Column(name = "gstin")
    private String gstin;
    @Column(name = "sac")
    private String sac;

    @Column(name = "creation_date")
    private Date creationDate;

    @Column(name = "last_modified")
    private Date lastModified = new Date();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client")
    private List<ClientLedger> clientLedgers = new ArrayList<ClientLedger>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client")
    private List<Estimate> estimates = new ArrayList<Estimate>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client")
    private List<ClientPackage> clientPackages = new ArrayList<ClientPackage>();
}
