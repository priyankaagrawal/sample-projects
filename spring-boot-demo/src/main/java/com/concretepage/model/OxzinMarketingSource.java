package com.concretepage.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer", "sourceCategory"})
@Table(name = "oxzin_marketing_source")
public class OxzinMarketingSource implements Serializable{
    @Id
    @Column(unique = true, name = "id")
    private Integer id;
    @Column(name="name")
    private String name;
    @Column(name="status")
    private StatusEnum status;
}
