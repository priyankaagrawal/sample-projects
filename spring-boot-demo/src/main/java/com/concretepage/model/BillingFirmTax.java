package com.concretepage.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer", "billingFirm"})
@Table(name = "billing_firm_tax")
public class BillingFirmTax implements Serializable{
    @Id
    @Column(unique = true, name = "id")
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "billing_firm", nullable = false)
    private BillingFirm billingFirm;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "tax", nullable = false)
    private Tax tax;
}
