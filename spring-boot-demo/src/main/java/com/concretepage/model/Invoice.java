package com.concretepage.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer","client", "estimate","clientPackages","clientLedgers"})
@Table(name = "source")
public class Invoice implements Serializable{
    @Id
    @Column(unique = true, name = "id")
    private Integer id;

    @Column(name = "invoice_date")
    private Date invoiceDate;
    @Column(name = "estimate_no")
    private String estimateNo;
    @Column(name = "invoice_no")
    private String invoiceNo;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "estimate", nullable = false)
    private Estimate estimate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client", nullable = false)
    private Client client;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "billingFirm", nullable = false)
    private BillingFirm billingFirm;


    @Column(name = "terms")
    private String terms;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "invoice")
    private List<ClientLedger> clientLedgers = new ArrayList<ClientLedger>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "invoice")
    private List<ClientPackage> clientPackages = new ArrayList<ClientPackage>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "invoice")
    private List<InvoiceTax> invoiceTaxes = new ArrayList<InvoiceTax>();

    @Column(name = "creation_date")
    private Date creationDate;

    @Column(name = "last_modified")
    private Date lastModified = new Date();

}
