package com.concretepage.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer", "convertedLeads"})
@Table(name = "treatment")
public class Treatment implements Serializable{
    @Id
    @Column(unique = true, name = "id")
    private Integer id;
    @Column(name="name")
    private String name;
    @Column(name="status")
    private StatusEnum status;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "treatment")
    private List<ClientPackageTreatment> clientPackageTreatments = new ArrayList<ClientPackageTreatment>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "treatment")
    private List<ConvertedLead> convertedLeads = new ArrayList<ConvertedLead>();
}
