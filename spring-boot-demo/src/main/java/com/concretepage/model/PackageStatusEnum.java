package com.concretepage.model;

public enum PackageStatusEnum {
    RUNNING,
    STOP,
    PAUSED,
    COMPLETED;
}
