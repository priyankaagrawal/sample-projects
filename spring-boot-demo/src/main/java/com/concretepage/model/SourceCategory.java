package com.concretepage.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer", "sources"})
@Table(name = "source_category")
public class SourceCategory implements Serializable {
    @Id
    @Column(unique = true, name = "id")
    private Integer id;
    @Column(name="name")
    private String name;
    @Column(name="status")
    private StatusEnum status;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "sourceCategory")
    private List<Source> sources = new ArrayList<Source>();

}
