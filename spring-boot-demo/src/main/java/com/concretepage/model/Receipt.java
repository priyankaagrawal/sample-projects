package com.concretepage.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer", "clientLedgers"})
@Table(name = "receipt")
public class Receipt implements Serializable {
    @Id
    @Column(unique = true, name = "id")
    private Integer id;

    @Column(name = "creation_date")
    private Date creationDate;

    @Column(name = "last_modified")
    private Date lastModified = new Date();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "receipt")
    private List<ClientLedger> clientLedgers = new ArrayList<ClientLedger>();


}