package com.concretepage.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Data
@NoArgsConstructor
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer", "clientPackage"})
@Table(name = "converted_lead")
public class ConvertedLead implements Serializable {

    @Id
    @Column(unique = true, name = "id")
    private Integer id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "clientPackage", nullable = false)
    private ClientPackage clientPackage;
    @Column(name = "creation_date")
    private Date creationDate;

    @Column(name = "last_modified")
    private Date lastModified = new Date();

    @Column(name = "lead_capture_date")
    private Date leadCaptureDate;

    @Column(name = "execution_date")
    private Date executionDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "source", nullable = false)
    private Source source;

    @Column(name = "patient_name")
    private String patientName;
    @Column(name = "mobile")
    private String mobile;
    @Column(name = "email")
    private String email;
    @Column(name = "city")
    private String city;
    @Column(name = "preferred_location")
    private String preferredLoction;
    @Column(name = "verified_by")
    private String verifiedBy;
    @Column(name = "booking_amount")
    private Double bookingAmount;
    @Column(name = "surgery_amount")
    private String surgeryAmount;
    @Column(name = "surgery_date")
    private Date surgeryDate;
    @Column(name = "booking_date")
    private Date bookingDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "treatment", nullable = false)
    private Source treatment;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "center", nullable = false)
    private Center center;

}
