package com.concretepage.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Priyanka on 3/6/2015.
 */
@Entity
@Data
@NoArgsConstructor
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer","respondents"})
@Table(name = "user")
public class User implements Serializable {

    public final static String LOGIN_FACEBOOK = "FACEBOOK";
    public final static String LOGIN_GOOGLE = "GOOGLE";
    public final static String LOGIN_DIRECT = "DIRECT";

    @Id
    @Column(unique = true, name = "username")
        private String username;

    @Column(unique = true, name = "password")
    private String password;

    @Column
    private String token;



    private String name;

    @Column(name = "is_encrypted")
    private Integer isEncrypted = 0;


    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = {CascadeType.ALL}, orphanRemoval = true)
    private Set<UserRole> userRoles;


    public void addUserRole(UserRole userRole) {
        if (getUserRoles() == null) {
            setUserRoles(new HashSet<UserRole>());
        }
        getUserRoles().add(userRole);
    }


    public User(String username) {
        this.username = username;
    }

    public User(String username, String password) {
        this.username = username;
        this.password=password;
    }

}
