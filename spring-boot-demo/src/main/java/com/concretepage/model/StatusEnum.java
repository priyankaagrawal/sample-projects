package com.concretepage.model;

import jdk.nashorn.internal.ir.Block;

public enum StatusEnum {
    ACTIVE,
    BLOCK,
    SHOW,
    HIDE;
}
