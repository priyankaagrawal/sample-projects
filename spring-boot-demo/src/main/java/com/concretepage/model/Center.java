package com.concretepage.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer", "clientPackageCenters","convertedLeads"})
@Table(name = "center")
public class Center implements Serializable {
    @Id
    @Column(unique = true, name = "id")
    private Integer id;
    @Column(unique = false, name = "name")
    private String name;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "client", nullable = false)
    private Client client;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "center")
    private List<ClientPackageCenter> clientPackageCenters = new ArrayList<ClientPackageCenter>();


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "center")
    private List<ConvertedLead> convertedLeads = new ArrayList<ConvertedLead>();
}
