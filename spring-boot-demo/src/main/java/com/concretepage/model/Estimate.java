package com.concretepage.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer", "client","billingFirm"})
@Table(name = "estimate")
public class Estimate implements Serializable{

    @Id
    @Column(unique = true, name = "id")
    private Integer id;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "estimate")
    private List<EstimateTax> estimateTaxes = new ArrayList<EstimateTax>();

    @Column(name = "creation_date")
    private Date creationDate;

    @Column(name = "last_modified")
    private Date lastModified = new Date();
    @Column(name = "estimate_no")
    private String estimateNo;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client", nullable = false)
    private Client client;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "billingFirm", nullable = false)
    private BillingFirm billingFirm;

}
