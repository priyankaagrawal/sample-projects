package com.concretepage.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer", "sourceCategory","weeklyPackageInvestments","convertedLeads","dailyLeadGenerateds"})
@Table(name = "source")
public class Source implements Serializable{
    @Id
    @Column(unique = true, name = "id")
    private Integer id;
    @Column(name="name")
    private String name;
    @Column(name="status")
    private StatusEnum status;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "source", nullable = false)
    private SourceCategory sourceCategory;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "source")
    private List<WeeklyPackageInvestment> weeklyPackageInvestments = new ArrayList<WeeklyPackageInvestment>();


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "source")
    private List<DailyLeadGenerated> dailyLeadGenerateds = new ArrayList<DailyLeadGenerated>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "source")
    private List<ConvertedLead> convertedLeads = new ArrayList<ConvertedLead>();
}
