package com.concretepage.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer", "clientPackageCenters","clientPackageTreatments",
        "dailyLeadAllotteds", "dailyLeadGenerateds","weeklyPackageInvestments","convertedLeads"})
@Table(name = "client_package")
public class ClientPackage implements Serializable{
    @Id
    @Column(unique = true, name = "id")
    private Integer id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "invoice", nullable = false)
    private Invoice invoice;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "package", nullable = false)
    private Package packages;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client", nullable = false)
    private Client client;
    @Column(name = "invoice_package_name")
    private String invoicePackageName;
    @Column(name = "start_date")
    private Date starDate;
    @Column(name = "end_date")
    private Date endDate;
    @Column(name = "actual_start_date")
    private Date actualStarDate;
    @Column(name = "actual_end_date")
    private Date actualEndDate;
    @Column(name = "creation_date")
    private Date creationDate;

    @Column(name = "last_modified")
    private Date lastModified = new Date();


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "clientPackage")
    private List<ClientPackageCenter> clientPackageCenters = new ArrayList<ClientPackageCenter>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "clientPackage")
    private List<ClientPackageTreatment> clientPackageTreatments = new ArrayList<ClientPackageTreatment>();

    @Column(name = "package_status")
    private PackageStatusEnum packageTypeStatus;

    @Column(name = "package_type")
    private PackageTypeEnum packageTypeEnum;

    @Column(name = "package_code")
    private String packageCode;

    @Column(name = "package_name")
    private String packageName;

    @Column(name = "no_of_lead")
    private Integer noOfleads;

    @Column(name = "recovery_lead")
    private Integer recoveryLeads;

    @Column(name = "offer_lead")
    private Integer offerLead;

    @Column(name = "replacement")
    private Integer replacement;

    @Column(name = "rate_per_lead")
    private Double ratePerLead;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "expected_lead_daily")
    private Integer expectedLeadDaily;

    @Column(name = "expected_budget")
    private Double expectedBudget;


    @Column(name = "payment_terms")
    private Double paymentTerms;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "clientPackage")
    private List<DailyLeadAllotted> dailyLeadAllotteds = new ArrayList<DailyLeadAllotted>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "clientPackage")
    private List<DailyLeadGenerated> dailyLeadGenerateds = new ArrayList<DailyLeadGenerated>();


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "clientPackage")
    private List<WeeklyPackageInvestment> weeklyPackageInvestments = new ArrayList<WeeklyPackageInvestment>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "clientPackage")
    private List<ConvertedLead> convertedLeads = new ArrayList<ConvertedLead>();

}
