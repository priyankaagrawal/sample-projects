package com.concretepage.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Data
@NoArgsConstructor
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer", "clientPackage"})
@Table(name = "daiy_lead_generated")
public class DailyLeadGenerated implements Serializable {


    @Id
    @Column(unique = true, name = "id")
    private Integer id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "clientPackage", nullable = false)
    private ClientPackage clientPackage;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "source", nullable = false)
    private Source source;

    @Column(name = "package_date")
    private Date packageDate;

    @Column(name = "no_of_lead_generated")
    private Integer noOfLeadGenerted;
    @Column(name = "date_added")
    private Date dateAdded;

    @Column(name = "last_modified")
    private Date lastModified = new Date();


}