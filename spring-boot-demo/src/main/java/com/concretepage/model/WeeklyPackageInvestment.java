package com.concretepage.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Data
@NoArgsConstructor
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer", "clientPackage"})
@Table(name = "weekly_package_investment")
public class WeeklyPackageInvestment implements Serializable {


    @Id
    @Column(unique = true, name = "id")
    private Integer id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "clientPackage", nullable = false)
    private ClientPackage clientPackage;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "source", nullable = false)
    private Source source;

    @Column(name = "weekly_start_date")
    private Date weeklyStartDate;


    @Column(name = "weekly_end_date")
    private Date weeklyEndDate;

    @Column(name = "no_of_lead_generated")
    private Integer noOfLeadGenerted;


}