package com.concretepage.core;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * Created by Priyanka on 07-03-2015.
 */

@Data
@EqualsAndHashCode(callSuper = false)
public class LoggedInUserDetails extends User {

    private static final long serialVersionUID = 2149394973496625539L;

    public LoggedInUserDetails(String userName, String password, Collection<? extends GrantedAuthority> authorities) {
        super(userName, password, authorities);
    }

    private String userName;
    private Long surveyorId;
    private Long respondentId;
}
