package com.concretepage.core;

import org.springframework.security.core.AuthenticationException;

/**
 * Created by Priyanka on 21-05-2015.
 */
public class BlockedUserException extends AuthenticationException {

    public BlockedUserException(String msg, Throwable t) {
        super(msg, t);
    }
    public BlockedUserException(String msg) {
        super(msg);
    }
}
