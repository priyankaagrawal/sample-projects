package com.concretepage.core;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Priyanka on 21-05-2015.
 */
@Component
public class LoginFailureHandler implements AuthenticationFailureHandler {


    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        ServletOutputStream out = response.getOutputStream();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("status", "error");
        if(exception instanceof BadCredentialsException){
            map.put("message","User/Password Incorrect.");
        }else{
            map.put("message", exception.getMessage());
        }
        ObjectMapper mapper = new ObjectMapper();
        response.setContentType("application/json; charset=UTF-8");
        mapper.writeValue(out, map);
    }
}
