package com.concretepage.core;


import com.concretepage.model.User;
import com.concretepage.repository.UserRepository;
import com.concretepage.service.UserService;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.security.SecureRandom;

/**
 * Created by Priyanka on 9/17/2015.
 */
@Service
public class TokenUtilsImpl implements TokenUtils {


    @Inject
    UserRepository userRepository;


    @Inject
    UserService userService;

    private String ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private SecureRandom RANDOM = new SecureRandom();

    /**
     * Generates a string token.
     */
    @Override
    public String generateToken(String username) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < 32; i++) {
            builder.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
        }
        return builder.toString();
    }

    @Override
    public UserDetails validate(String token, String username) {
        User login = userRepository.findByTokenAndUsername(token, username);
        if (login == null) {
            return null;
        }
//        Salesman salesman = salesmanRepository.findByUser(login);
//        if (salesman == null) {
//            return null;
//        }

        LoggedInUserDetails loggedInUserDetails = new LoggedInUserDetails(login.getUsername(), login.getPassword(), userService.getAuthorities(login.getUserRoles()));
        loggedInUserDetails.setUserName(login.getUsername());

//        if (salesman != null) {
//            loggedInUserDetails.setSalesmanId(salesman.getId());
//        }
        return loggedInUserDetails;
    }

}
