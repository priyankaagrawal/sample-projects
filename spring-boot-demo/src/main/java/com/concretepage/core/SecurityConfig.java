package com.concretepage.core;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.switchuser.SwitchUserFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.inject.Inject;

/**
 * Created by Priyanka on 06-03-2015.
 */
@Configuration
@EnableWebSecurity
@EnableScheduling
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Inject
    private UserDetailsService userDetailsService;

    @Inject
    CustomAuthenicationEntryPoint customAuthenicationEntryPoint;

    @Inject
    ApplicationContext applicationContext;


    @Inject
    LoginFailureHandler loginFailureHandler;

    @Inject
    CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler;

    @Inject
    AuthenticationTokenProcessingFilter authenticationTokenProcessingFilter;

    @Inject
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        DaoAuthenticationProvider daoAuthenticationProvider =
                new DaoAuthenticationProvider();
        daoAuthenticationProvider
                .setUserDetailsService(userDetailsService);
        auth.authenticationProvider(daoAuthenticationProvider);
        daoAuthenticationProvider.setPasswordEncoder(new BCryptPasswordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .anonymous().disable()
//                .addFilter(switchUserFilter)
                .authorizeRequests()
                .antMatchers("/j_spring_security_switch_user").access("hasRole('ROLE_ADMIN')")
                .antMatchers("/admin*/**").access("hasRole('ROLE_ADMIN')")

                .antMatchers("/api*/**").access("hasRole('ROLE_USER')  or hasRole('ROLE_ADMIN')  or hasRole('ROLE_RESPONDENT') ")
                .and()
                .httpBasic().authenticationEntryPoint(customAuthenicationEntryPoint)
                .and()
                .addFilterBefore(authenticationTokenProcessingFilter, BasicAuthenticationFilter.class)
//                .and()
                .formLogin().loginPage("/no-login").loginProcessingUrl("/login").successHandler(customAuthenticationSuccessHandler)//defaultSuccessUrl("/login-success")
//                .failureUrl("/login-failure").permitAll()
                .failureHandler(loginFailureHandler).permitAll()
                .and()
                .logout().logoutUrl("/logout").logoutSuccessUrl("/index.html");
    }
}
