package com.concretepage.core;

import org.springframework.security.core.userdetails.UserDetails;

/**
 * Created by Priyanka on 9/17/2015.
 */
public interface TokenUtils {
//    String getToken(UserDetails userDetails);
//    String getToken(UserDetails userDetails, Long expiration);

    UserDetails validate(String token, String username);

    String generateToken(String username);

//    UserDetails getUserFromToken(String token);
}
