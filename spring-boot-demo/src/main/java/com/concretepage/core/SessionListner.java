package com.concretepage.core;

/**
 * Created by Priyanka on 9/30/2015.
 */

import com.concretepage.service.SessionService;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class SessionListner implements HttpSessionListener {

    //private @Value("${sessionTime}") Integer sessionTime;
    private Integer sessionTime;
    
    @Override
    public void sessionCreated(HttpSessionEvent se) {
       
        sessionTime=getSessionTimeForWeb(se); 
        
//        System.out.println("Establishing web session to "+sessionTime+" seconds");
        se.getSession().setMaxInactiveInterval(sessionTime);
       
    }

    //for mobile app
//    @Override
//    public void sessionCreated(HttpSessionEvent se) {
//        se.getSession().setMaxInactiveInterval(720000);
//    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
//        System.out.println("Session time is set to "+sessionTime+" seconds");
//        System.out.println("Session destroyed");
    }
    
    /*
    * mty dev team
    * new method added
    * this metod is to get the value for the sesion defined on the application.properties file
    */
     private int getSessionTimeForWeb(HttpSessionEvent sessionEvent){
        HttpSession session = sessionEvent.getSession();
        ApplicationContext ctx = WebApplicationContextUtils.
                getWebApplicationContext(session.getServletContext());
        SessionService sessionService =
                (SessionService) ctx.getBean("sessionService");              
        return sessionService.getSessionTimeForWeb();
    }
}
