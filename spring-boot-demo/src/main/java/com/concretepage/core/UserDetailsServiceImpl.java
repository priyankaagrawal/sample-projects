package com.concretepage.core;


import com.concretepage.model.User;
import com.concretepage.repository.UserRepository;
import com.concretepage.service.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Priyanka on 06-03-2015.
 */
@Service
@Named("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Inject
    UserRepository userRepository;

    @Inject
    UserService userService;



    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException, BlockedUserException {
        User login = null;
        login = userRepository.findByUsername(s);
        if (login == null) {
            throw new UsernameNotFoundException("Username not found : " + s);
        }


//        Surveyor surveyor = surveyorRepository.findByUser(login);
//        Respondent respondent = respondentRepository.findByUser(login);
//        if (surveyor != null && surveyor.getStatus() == StatusEnum.BLOCKED) {
//            throw new BlockedUserException("User Blocked: " + s);
//        } else if (respondent != null && respondent.getStatus() == StatusEnum.BLOCKED) {
//            throw new BlockedUserException("User Blocked: " + s);
//        }

        LoggedInUserDetails loggedInUserDetails = new LoggedInUserDetails(login.getUsername(), login.getPassword(), userService.getAuthorities(login.getUserRoles()));
        loggedInUserDetails.setUserName(login.getUsername());
//        if (surveyor != null) {
//            loggedInUserDetails.setSurveyorId(surveyor.getId());
//        }
//        if (respondent != null) {
//            loggedInUserDetails.setRespondentId(respondent.getId());
//        }
        return loggedInUserDetails;
    }
}