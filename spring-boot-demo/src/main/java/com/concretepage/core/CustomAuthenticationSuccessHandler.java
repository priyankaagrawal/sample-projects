package com.concretepage.core;


import com.concretepage.model.User;
import com.concretepage.repository.UserRepository;
import com.concretepage.utils.CoreUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Priyanka on 9/17/2015.
 */
@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    @Inject
    TokenUtils tokenUtils;

    @Inject
    UserRepository userRepository;


    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest,
                                        HttpServletResponse httpServletResponse,
                                        Authentication authentication)
            throws IOException, ServletException {
        //do some logic here if you want something to be done whenever
        //the user successfully logs in.

        HttpSession session = httpServletRequest.getSession();

        LoggedInUserDetails loggedInUserDetails = (LoggedInUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User authUser = userRepository.findByUsername(loggedInUserDetails.getUsername());
        session.setAttribute("username", authUser.getUsername());
        session.setAttribute("authorities", authentication.getAuthorities());

        //set our response to OK status
        httpServletResponse.setStatus(HttpServletResponse.SC_OK);

        String token = tokenUtils.generateToken(authUser.getUsername());
        authUser.setToken(token);
        userRepository.save(authUser);

        httpServletResponse.setHeader("X-TOKEN", token);
        httpServletResponse.setHeader("X-ID", authUser.getUsername() + "");

        //since we have created our custom success handler, its up to us to where
        //we will redirect the user after successfully login
//        httpServletResponse.sendRedirect("login-success");
        httpServletResponse.setContentType("application/json; charset=UTF-8");

        Map<String, Object> model = new HashMap<>();
        model.put("status", "success");
        model.put("message", "Login Successful");
        model.put("user", CoreUtils.objectToJson(authUser));
        model.put("surveyorId", loggedInUserDetails.getSurveyorId());
        model.put("respondentId", loggedInUserDetails.getRespondentId());

        PrintWriter printWriter = httpServletResponse.getWriter();
        printWriter.write(CoreUtils.objectToJson(model));
        printWriter.flush();
    }
}
