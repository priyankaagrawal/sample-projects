package com.concretepage.repository;

import com.concretepage.model.User;
import com.concretepage.model.UserRole;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Priyanka on 6/8/2015.
 */
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {
public UserRole findByUser(User User);
}
