package com.concretepage.repository;

import com.concretepage.model.User;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, String> {

    public User findByUsername(String username);

    public List<User> findByIsEncrypted(Integer isEncrypted);

    public User findByTokenAndUsername(String token, String username);
    public User findByUsernameAndPassword(String username, String password);
}
