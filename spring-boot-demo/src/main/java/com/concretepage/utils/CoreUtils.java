package com.concretepage.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.util.Random;

/**
 * Created by Priyanka on 07-03-2015.
 */
@Component
public class CoreUtils {

    static DecimalFormat df = new DecimalFormat("#0.00");

    public static Long generateRandomCode() {
        long random;
        final long MAX = 999999,
                MIN = 100000;
        random = (long) (Math.floor(Math.random() * (MAX - MIN + 1)) + MIN);
        return random;
    }

    public static String formatDouble(Double d) {
        if (d == null) {
            return "0.00";
        }
        return df.format(d);
    }

    public static String objectToJson(Object o) {
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
        return json;
    }

    static public String encodePassword(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(password);
        return hashedPassword;
    }

    static public Boolean matchPassword(String rawPassword, String encodedPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        Boolean aBoolean = passwordEncoder.matches(rawPassword, encodedPassword);
        return aBoolean;
    }

    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    static Random rnd = new Random();

    static public String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }


}
