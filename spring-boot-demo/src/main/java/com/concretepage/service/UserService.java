package com.concretepage.service;


import com.concretepage.model.User;
import com.concretepage.model.UserRole;

import org.springframework.security.core.GrantedAuthority;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Priyanka on 15-03-2015.
 */
public interface UserService {

    public List<GrantedAuthority> getAuthorities(Set<UserRole> userRoles);

    public User
    getLoggedInUser();

    public  Map<String, Object> getUsers();

  }

