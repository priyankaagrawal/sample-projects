/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.concretepage.service;

/**
 *
 * @author Manuel de la Cruz
 * created on feb 13 to read the session timeout from the application.properties file
 */
public interface SessionService {
    
    int getSessionTimeForWeb();
    int getSessionTimeForMobile();
    
}
