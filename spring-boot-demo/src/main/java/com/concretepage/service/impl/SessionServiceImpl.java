/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.concretepage.service.impl;

import com.concretepage.service.SessionService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author Manuel de la Cruz
 * feb 13 2016
 * Service used to read the value to be set as the session timeout for web access and for mobile access
 */
@Service("sessionService")
public class SessionServiceImpl implements SessionService {
    
    private @Value("${sessionTime}")Integer sessionTime;
    
    private @Value("${sessionTime.mobile}") Integer sessionTimeMobile;
    
    @Override
    public int getSessionTimeForWeb(){
        return sessionTime;        
    }
    
    @Override
    public int getSessionTimeForMobile(){
        return sessionTimeMobile;
    }

    public Integer getSessionTime() {
        return sessionTime;
    }

    public void setSessionTime(Integer sessionTime) {
        this.sessionTime = sessionTime;
    }

    public Integer getSessionTimeMobile() {
        return sessionTimeMobile;
    }

    public void setSessionTimeMobile(Integer sessionTimeMobile) {
        this.sessionTimeMobile = sessionTimeMobile;
    }
    
    
    
}
