package com.concretepage.service.impl;

import com.concretepage.core.LoggedInUserDetails;
import com.concretepage.model.User;
import com.concretepage.model.UserRole;
import com.concretepage.repository.UserRepository;
import com.concretepage.service.UserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.*;

/**
 * Created by Priyanka on 15-03-2015.
 */
@Service
public class UserServiceImpl implements UserService {

    @Inject
    UserRepository userRepository;


    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Map<String, Object> getUsers() {
        Map<String, Object> map = new HashMap<>();
        List<User> users = userRepository.findAll();
//        List<AdminUserBean> adminUserBeanList = new ArrayList<>();
//        for (User u : users) {
//            AdminUserBean adminUserBean = new AdminUserBean();
//            adminUserBean.setUsername(u.getUsername());
//            Integer countSalesman = 0;
//            Salesman salesman = salesmanRepository.findByUser(u);
//            if (salesman != null) {
//                adminUserBean.setSalesman(salesman.getName());
//                adminUserBean.setSalesmanType(salesman.getSalesmanType().getDescription());
//                countSalesman = salesmanRepository.findBySalesmanagerId(salesman.getId()).size();
//
//            } else {
//
//                adminUserBean.setSalesman("");
//                adminUserBean.setSalesmanType("");
//            }
//            adminUserBean.setTotalSalesman(countSalesman);
//            adminUserBeanList.add(adminUserBean);
//        }
//
//        map.put("content", adminUserBeanList);

        map.put("recordsTotal", users.size());
        map.put("recordsFiltered", users.size());
        return map;
    }

//    @Override
//    public void putUserInSession(User login) {
//        LoggedInUserDetails ll = new LoggedInUserDetails(login.getUsername(), login.getPassword(), getAuthorities(login.getUserRoles()));
//        ll.setUserName(login.getUsername());
//        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(ll, login.getPassword(), getAuthorities(login.getUserRoles())));
//    }


    @Transactional
    public User getLoggedInUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = (UsernamePasswordAuthenticationToken) auth;
        LoggedInUserDetails loggedInUserDetails = (LoggedInUserDetails) usernamePasswordAuthenticationToken.getPrincipal();

        User user = userRepository.findByUsername(loggedInUserDetails.getUsername());
        return user;
    }


    public List<GrantedAuthority> getAuthorities(Set<UserRole> userRoles) {
        List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
        if (userRoles != null && !userRoles.isEmpty()) {
            for (UserRole userRole : userRoles) {
                authList.add(new SimpleGrantedAuthority(userRole.getType()));
                // If Permission is not null create a granted authorities for it
                if (userRole.getPermission() != null) {
                    authList.add(new SimpleGrantedAuthority(userRole.getPermission()));
                }
            }
        }
        return authList;
    }


}
