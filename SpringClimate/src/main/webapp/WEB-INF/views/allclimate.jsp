<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<div class="row">
    <div class="col-md-4 col-md-offset-1 ">
        <h2>Current Temperature</h2> <br/>
        <div class="bordered">

            <div class="row">
                <div class="col-md-6"><h3>In Celsius</h3></div>
                <div class="col-md-6"><h3>${climateModel.tempCelsius}</h3></div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-md-6"><h3>In Farenheit</h3></div>
                <div class="col-md-6"><h3>${climateModel.tempFarenheit}</h3></div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-md-6"><h3>Conditions</div>
                <div class="col-md-6"><h3>${climateModel.conditions}</h3></div>
            </div>
        </div>
    </div>
    <div class="col-md-7">
        <h2>Climate History</h2> <br/>
        <table class="table table-bordered table striped">
            <tr>
                <td>Date</td>
                <td>Temp Celsius</td>
                <td>Temp Farhenit</td>
                <td>Condition</td>

            </tr>
            <c:forEach items="${climates}" var="climate">
                <tr>

                    <td>${climate.creationDate}</td>
                    <td>${climate.tempCelsius}</td>
                    <td>${climate.tempFarenheit}</td>
                    <td>${climate.conditions}</td>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>


