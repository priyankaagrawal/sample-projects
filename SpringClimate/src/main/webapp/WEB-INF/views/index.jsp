<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2/28/2017
  Time: 7:35 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>

    <style type="text/css">
        .bordered{
            border: 5px solid #337ab7;
            border-radius: 20px;
            padding: 20px;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#" style=" color: #fff;">Climate Measuring</a>
        </div>
    </div>
</nav>

<div class="jumbotron">
    <div class="container">
<br/><br/>
            <div class="row">
                <div class="col-md-6">
                <div class="form-group">
                    <label for="city" class="control-label col-md-3">Select City</label>
                    <div class="col-md-9">
                    <select id="city" name="city" class="form-control">
                        <option value="0">Select</option>
                        <option value="1">Paris</option>
                        <option value="2">Indore</option>
                    </select>
                        </div>
                </div>
                </div>
                <div class="col-md-2">
                <button type="submit" id="listView" class="btn btn-primary">
                         Submit</button>
                </div>
                <div class="col-md-2">
                    <div class="loader" style="display: none;">
                        Loading..............
                    </div>
                </div>
            </div>



    </div>
</div>


<div class="container">
    <div id="result"></div>

    </div>
    <script type="text/javascript">
        jQuery("#listView").click(function (e) {
            e.preventDefault();
            jQuery(".loader").css("display", "block");
            if($("#city").val()<=0)
            {
                alert("Select City");
                jQuery(".loader").css("display", "none");
                return;
            }else {
                $.ajax({
                    type: "get",
                    url: "/list",
                    data: {
                        "city": $("#city").val()
                    },
                    success: function (response) {
                        $('#result').html(response);
                        jQuery(".loader").css("display", "none");
                    },
                    error: function () {
                        alert('Error while request..');
                        jQuery(".loader").css("display", "none");
                    }
                });
            }
        })

    </script>

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
