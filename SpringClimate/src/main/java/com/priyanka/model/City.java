package com.priyanka.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 7/17/2015.
 */
@Entity
@Data
@NoArgsConstructor
@Table(name = "city")
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer","climate"})
public class City {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "city_id")
    private int id;
    private String name;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "city")
    private List<Climate> climate = new ArrayList<Climate>();

}
