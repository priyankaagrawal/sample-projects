package com.priyanka.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.validation.Valid;

import com.priyanka.model.Climate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import com.priyanka.model.Climate;
import com.priyanka.service.ClimateService;

@Controller
@RequestMapping("/")
public class AppController {

    @Autowired
    ClimateService service;

    @Autowired
    MessageSource messageSource;

    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String listClimates() {

        return "index";
    }


    /*
     * This method will list all existing climate.
     */
    @RequestMapping(value = {"/list"}, method = RequestMethod.GET)
    public String listClimate(ModelMap model, @RequestParam(value = "city", required = true, defaultValue = "0") int city) {


        Climate climate = service.getClimateByApi(city);
        model.addAttribute("climateModel", climate);

        List<Climate> climates = service.findClimateByCity(city);
        model.addAttribute("climates", climates);
        return "allclimate";
    }


}
