package com.priyanka.service;

import java.util.List;

import com.priyanka.model.Climate;

public interface ClimateService {



	List<Climate> findAllClimates();

	List<Climate>  findClimateByCity(int city);


Climate getClimateByApi(int city);
	
}
