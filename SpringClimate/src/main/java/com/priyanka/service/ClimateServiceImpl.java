package com.priyanka.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.priyanka.bean.ClimateApi;
import com.priyanka.dao.CityDao;
import com.priyanka.model.City;
import com.priyanka.model.Climate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.priyanka.dao.ClimateDao;

@Service("climateService")
@Transactional
public class ClimateServiceImpl implements ClimateService {

	@Autowired
	private ClimateDao dao;

	@Autowired
	private CityDao cityDao;

	public List<Climate> findAllClimates() {
		return dao.findAllClimates();
	}

	public List<Climate> findClimateByCity(int city) {
		return dao.findClimateByCity(city);
	}


	public Climate getClimateByApi(int city)
	{
		Climate climate= new Climate();
		City cityModel= cityDao.findById(city);
		try {
			String USER_AGENT = "Mozilla/5.0";
			URL obj = new URL("http://api.apixu.com/v1/current.json"+"?key=b19ee63d0f5b497ea47111349172802&q="+cityModel.getName());
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			//add reuqest header
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
//			con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");

			String urlParameters ="" ;
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();
//			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Post parameters : " + urlParameters);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(
					new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			ClimateApi climateApi= convertToBean(String.valueOf(response));

			climate.setCity(cityModel);
			climate.setCreationDate(new Date());
			climate.setConditions(climateApi.getCurrent().getCondition().getText());
			climate.setTempCelsius(climateApi.getCurrent().getTemp_c());
			climate.setTempFarenheit(climateApi.getCurrent().getTemp_c());
			dao.saveClimate(climate);

		}catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return  climate;
	}

	private ClimateApi convertToBean(String json)
	{
		ClimateApi climateApi=null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			climateApi	 = mapper.readValue(json, ClimateApi.class);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return climateApi;
	}
	
}
