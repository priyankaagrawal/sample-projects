package com.priyanka.dao;

import com.priyanka.model.City;
import com.priyanka.model.Climate;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("cityDao")
public class CityDaoImpl extends AbstractDao<Integer, City> implements CityDao {

	public City findById(int id) {
		return getByKey(id);
	}


}
