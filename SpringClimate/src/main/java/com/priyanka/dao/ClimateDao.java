package com.priyanka.dao;

import java.util.List;

import com.priyanka.model.Climate;

public interface ClimateDao {

	Climate findById(int id);

	void saveClimate(Climate climate);



	List<Climate> findAllClimates();

	List<Climate> findClimateByCity(int City);

}
