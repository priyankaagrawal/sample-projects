package com.priyanka.dao;

import com.priyanka.model.City;
import com.priyanka.model.Climate;

import java.util.List;

public interface CityDao {

	City findById(int id);

}
