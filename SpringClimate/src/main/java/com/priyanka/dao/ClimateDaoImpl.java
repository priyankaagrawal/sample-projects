package com.priyanka.dao;

import java.util.List;

import com.priyanka.model.City;
import com.priyanka.model.Climate;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("climateDao")
public class ClimateDaoImpl extends AbstractDao<Integer, Climate> implements ClimateDao {

	public Climate findById(int id) {
		return getByKey(id);
	}

	@Transactional
	public void saveClimate(Climate climate) {
		persist(climate);
	}

	@Autowired
	CityDao cityDao;

	@SuppressWarnings("unchecked")
	public List<Climate> findAllClimates() {
		Criteria criteria = createEntityCriteria();
		return (List<Climate>) criteria.list();
	}

	public List<Climate> findClimateByCity(int cityId) {
		Criteria criteria = createEntityCriteria();
		City city= cityDao.findById(cityId);
		criteria.add(Restrictions.eq("city", city));
		return (List<Climate>) criteria.list();
	}
}
