package com.priyanka.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Created by Administrator on 2/28/2017.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClimateApi {
    ClimateLocation location;
    ClimateCurrent current;
}
