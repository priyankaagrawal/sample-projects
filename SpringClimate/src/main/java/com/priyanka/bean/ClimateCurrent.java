package com.priyanka.bean;

import lombok.Data;

/**
 * Created by Administrator on 2/28/2017.
 */
@Data
public class ClimateCurrent {
    ClimateCondition condition;
    private String temp_c;
    private String temp_f;

}
